#!/usr/bin/python
import time
import random

import world

class Player():
	def __init__(self, x, y, z):
		self.playerX = x
		self.playerY = y
		self.playerZ = z

class GameConfig():
	def __init__(self, config):
		self.worldSize = config.getInt("dcrawlWorldSize")
		self.levelXSize = config.getInt("dcrawlLevelXSize")
		self.levelYSize = config.getInt("dcrawlLevelYSize")
		self.manualURI = config.getStr("dcrawlManualURI")

class DungeonCrawl():
	def __init__(self, config, connection, pasteBin):
		self.gameConfig = GameConfig(config)
		self.db_manager = config.db_manager
		self.connection = connection
		self.pasteBin = pasteBin
		self.gameStarted = False
		self.gameWorld = None
		self.player = None

	def startGame(self):
		self.connection.sendMessage("Starting dcrawl! CHAT, GET READY TO ESCAPE THE DUNGEON!")
		self.gameStarted = True
		self.player = Player(random.randint(0, self.gameConfig.levelXSize - 1), random.randint(0, self.gameConfig.levelYSize - 1), 0)
		self.gameWorld = world.World(self.gameConfig)
		self.gameWorld.setSpawn(self.player)

		print "Game Started, player position: " + str(self.player.playerX) + ", " + str(self.player.playerY) + ", " + str(self.player.playerZ)

	def endGame(self):
		self.gameStarted = False
		self.gameWorld = None
		self.player = None

	def winGame(self, nick):
		self.connection.sendMessage("You scramble out of the pit and onto the warm clay, panting and squinting as the smells and sounds of the countryside wash over you. As your eyes adjust, you see a wonderous sight. You see your freedom.")
		self.connection.sendMessage("Congratulations, Chat, you escaped!! The winning move was performed by: " + nick)
		self.endGame()

	def handleInput(self, nick, full_msg):
		
		if len(full_msg) > 1:
			if self.gameStarted:
				currentRoom = self.gameWorld.levels[self.player.playerZ].rooms[self.player.playerX][self.player.playerY]
				if "east" == full_msg[1]:
					if currentRoom.exits.east:
						self.player.playerX = self.player.playerX - 1
						self.connection.sendMessage("Moving east")
					else:
						self.connection.sendMessage("There is no exit to the east!")
				elif "west" == full_msg[1]:
					if currentRoom.exits.west:
						self.player.playerX = self.player.playerX + 1
						self.connection.sendMessage("Moving west")
					else:
						self.connection.sendMessage("There is no exit to the west!")
				elif "north" == full_msg[1]:
					if currentRoom.exits.north:
						self.player.playerY = self.player.playerY - 1
						self.connection.sendMessage("Moving north")
					else:
						self.connection.sendMessage("There is no exit to the north!")
				elif "south" == full_msg[1]:	
					if currentRoom.exits.south:
						self.player.playerY = self.player.playerY + 1
						self.connection.sendMessage("Moving south")
					else:
						self.connection.sendMessage("There is no exit to the south!")
				elif "up" == full_msg[1]:
					if currentRoom.upExit:
						if self.player.playerZ < self.gameConfig.worldSize - 1:
							self.player.playerZ = self.player.playerZ + 1
							self.gameWorld.setSpawn(self.player)
							self.connection.sendMessage("Moving Up, you are on floor: " + str(self.player.playerZ))
						else:
							self.winGame(nick)
					else:
						self.connection.sendMessage("There are no stairs going up here!")

				elif "look" == full_msg[1]:
					if len(full_msg) >= 2:
						print "Getting desc for room at Z,X,Y: " + str(self.player.playerZ) + ", " + str(self.player.playerX) + ", " + str(self.player.playerY)
						self.connection.sendMessage(currentRoom.getDesc())

				elif "map" == full_msg[1]:
					mapList = self.gameWorld.getTextMap(self.player.playerZ, self.player.playerX, self.player.playerY)
					for line in mapList:
						print line
						self.connection.sendMessage(line)
			else:
				if "start" == full_msg[1]:
					self.startGame()
		else:
			self.connection.sendMessage("DCrawl is a game! Read about it her: " + self.gameConfig.manualURI)