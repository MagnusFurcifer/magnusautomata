#!/usr/bin/python

import random

class Exits():
	def __init__(self):
		self.north = False
		self.south = False
		self.east = False
		self.west = False

class Room():
	def __init__(self, x, y):
		'''
		Room Types:
		1 = Normal
		2 = Exits
		3 = Entrace (Spawn)

		'''
		self.roomType = None
		self.roomDescription = "Emtpy"
		self.roomEnemies = list()
		self.exits = Exits()
		self.upExit = False

	def getDesc(self):
		tmp = self.roomDescription + " "
		if not self.roomEnemies:
			tmp = tmp + "There are no enemies here. "
		tmp = tmp + "The exits are:"
		if self.exits.north:
			tmp = tmp + " north"
		if self.exits.south:
			tmp = tmp + " south"
		if self.exits.east:
			tmp = tmp + " east"
		if self.exits.west:
			tmp = tmp + " west"
		if self.upExit:
			tmp = tmp + " up"
		return tmp

class Level():
	def __init__(self):
		self.rooms = list()


class LevelGenerator():
	def __init__(self, gameConfig):
		self.gameConfig = gameConfig
		self.level = Level()
		self.initLevelLists()
		self.generateLevel()

	def getLevel(self):
		return self.level

	def initLevelLists(self):
		print "Initalizing Levels"
		roomList = [[Room(x, i) for x in range(self.gameConfig.levelXSize)] for i in range(self.gameConfig.levelYSize)]
		self.level.rooms = roomList

	def generateLevel(self):
		#In the mean time, lets fill it up
		for y in range(0, self.gameConfig.levelXSize):
			for x in range(0, self.gameConfig.levelYSize):
				self.level.rooms[x][y].roomType = 1
				self.level.rooms[x][y].roomDescription = "This is an empty room at " + str(x) + ", " + str(y)
				if x > 0:
					self.level.rooms[x][y].exits.east = True
				if x < self.gameConfig.levelXSize - 1:
					self.level.rooms[x][y].exits.west = True
				if y > 0:
					self.level.rooms[x][y].exits.north = True
				if y < self.gameConfig.levelYSize - 1:
					self.level.rooms[x][y].exits.south = True

		#Find random exit point:
		randX = random.randint(0, self.gameConfig.levelXSize - 1)
		randY = random.randint(0, self.gameConfig.levelYSize - 1)
		print "The exit for this floor is at x,y: " + str(randX) + ", " + str(randY)
		self.level.rooms[randX][randY].roomType = 2
		self.level.rooms[randX][randY].upExit = True
		self.level.rooms[randX][randY].roomDescription = "You are in a stairwell, there are stairs ascending into darkness."