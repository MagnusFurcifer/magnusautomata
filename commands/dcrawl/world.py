#!/usr/bin/python


import level

class World():
	def __init__(self, gameConfig):
		self.gameConfig = gameConfig
		self.levels = list()
		self.generateWorld()

	def generateWorld(self):
		for i in range(0, self.gameConfig.worldSize):
			tmpGen = level.LevelGenerator(self.gameConfig)
			self.levels.append(tmpGen.getLevel())

	def setSpawn(self, player):
		self.levels[player.playerZ].rooms[player.playerX][player.playerY].roomType = 3
		if player.playerZ == 0:
			tmpDesc = "You have awakened in a dark room, it smells of mold and death."
		else:
			tmpDesc = "You are in a stairwell, below you the stairwell stretches into the abyss."
		self.levels[player.playerZ].rooms[player.playerX][player.playerY].roomDescription = tmpDesc

	def getTextMap(self, playerZ, playerX, playerY):
		#textMap = "X = Exit, O = Normal, P = Player, S = Spawn\r\n"
		textMap = list()
		for y in range(0, self.gameConfig.levelXSize):
			lineMap = ""
			for x in range(0, self.gameConfig.levelYSize):
				if x == playerX and y == playerY:
					lineMap = lineMap + "[P]"
				elif self.levels[playerZ].rooms[x][y].roomType == 1:
					lineMap = lineMap + "[O]"
				elif self.levels[playerZ].rooms[x][y].roomType == 2:
					lineMap = lineMap + "[X]"
				elif self.levels[playerZ].rooms[x][y].roomType == 3:
					lineMap = lineMap + "[S]"
			textMap.append(lineMap)
		return textMap
