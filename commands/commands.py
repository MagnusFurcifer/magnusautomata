#!/usr/bin/python

import raffle
import dcrawl
import paste
import random

class Command():
	def __init__(self):
		self.cmdInput = None
		self.cmdOutput = None
		self.cmdUsage = None
		self.cmdMod = None
	def getInput(self):
		return self.cmdInput
	def getOutput(self):
		return self.cmdOutput
	def getUsage(self):
		return self.cmdUsage
	def getMod(self):
		return self.cmdMod

class CMDManager():
	def __init__(self, config, connection):
		self.db_manager = config.db_manager
		self.cmdList = list()
		self.cmdListStatic = list()
		self.connection = connection
		self.pasteBin = paste.Paste(config)
		self.rafMan = raffle.RaffleManager(config, connection)
		self.dungeonCrawl = dcrawl.DungeonCrawl(config, connection, self.pasteBin)
		self.loadCommandsFromDB()
		self.loadStaticCommands()

	def update(self):
		self.rafMan.update()

	def isInt(self, string):
		try:
			int(string)
			return True
		except ValueError:
			return False

	def loadStaticCommands(self):
		self.cmdListStatic.append("!addcmd")
		self.cmdListStatic.append("!listcmds")
		self.cmdListStatic.append("!man")
		self.cmdListStatic.append("!raffle")


	def loadCommandsFromDB(self):
		commands = self.db_manager.selectAll("Commands")
		for command in commands:
			tmpCmd = Command()
			tmpCmd.cmdInput = command[1]
			tmpCmd.cmdOutput = command[2]
			tmpCmd.cmdUsage = command[3]
			tmpCmd.cmdMod = command[4]
			self.cmdList.append(tmpCmd)
			print "Loading Command: " + tmpCmd.cmdInput + " " + tmpCmd.cmdOutput + " " + tmpCmd.cmdUsage + " " + str(tmpCmd.cmdMod)

	def isUserMod(self, nick):
		print "Mod testing"
		for mod in self.connection.getMods():
			print "Is " + mod + " the same as " + nick
			if mod == nick:
				return True
		return False


	def parseMessage(self, nick, message):
		if "!" in message.split(" ")[0]: #Only proceed if the ! is found
			full_msg = message.split(" ")

			######################################RAFFLE####################################
			if not self.rafMan.activeRaffle:
				if self.isUserMod(nick):
					if "!raffle" == full_msg[0]:
						if "!" in full_msg[1]:
							self.rafMan.raffleEnterCmd = full_msg[1]
							if self.isInt(full_msg[2]):
								self.rafMan.raffleTime = full_msg[2]
								self.rafMan.startRaffle()
			elif self.rafMan.activeRaffle:
				if self.rafMan.raffleEnterCmd == full_msg[0]:
					print "User " + nick + " has entered the raffle"
					self.rafMan.users.append(nick)
			###############################################################################

			if self.isUserMod(nick):
				#######################################STATIC CMDS###########################################
				if "!addcmd" == full_msg[0]:
					tmp = AddCommand(nick, full_msg, message, self.db_manager, self.cmdList)
					self.connection.sendMessage(tmp.getOutput())
					return True
				elif "!deletecmd" == full_msg[0]:
					tmp = DeleteCommand(nick, full_msg, self.db_manager, self.cmdList)
					tmp.execute()
					self.loadCommandsFromDB()
					self.connection.sendMessage(tmp.getOutput())
					return True
				elif "!editcmd" == full_msg[0]:
					tmp = DeleteCommand(nick, full_msg, self.db_manager, self.cmdList)
					tmp.execute()
					self.loadCommandsFromDB()
					tmp2 = AddCommand(nick, full_msg, message, self.db_manager, self.cmdList)
					self.connection.sendMessage(tmp.getOutput())
					return True
				elif "!listcmds" == full_msg[0]:
					tmp = ListCommands(nick, self.cmdList, self.cmdListStatic)
					self.connection.sendMessage(tmp.getOutput())
					return True
				elif "!man" == full_msg[0]:
					tmp = ManPage(nick, full_msg, self.cmdList, self.cmdListStatic)
					self.connection.sendMessage(tmp.getOutput())
					return True
				elif "!luckydip" == full_msg[0]:
					tmp = LuckyDip(nick, full_msg, self.db_manager)
					self.connection.sendMessage(tmp.getOutput())
				#####################################################################################

			#####################################DYNAMIC CMDS##########################################
			for cmd in self.cmdList:
				if cmd.cmdInput == full_msg[0]:
					canProceed = False
					if cmd.cmdMod == 1:
						if self.isUserMod(nick):
							canProceed = True
					else:
						canProceed = True

					if canProceed:
						cmdOutput = cmd.cmdOutput
						replaceN = 1
						while cmdOutput.find("%s") is not -1:
							cmdOutput = cmdOutput.replace("%s", full_msg[replaceN], 1)
							replaceN = replaceN + 1
						self.connection.sendMessage(cmdOutput)
						return True
			########################################################################################




			#DCRAWL
			if "!dcrawl" == full_msg[0]:
				self.dungeonCrawl.handleInput(nick, full_msg)

		return False

class LuckyDipKey():
	def __init__(self):
		self.service = None
		self.key = None
		self.keyID = None

class LuckyDip():
	def __init__(self, nick, full_msg, db_manager):
		self.nick = nick
		self.full_msg = full_msg
		self.db_manager = db_manager
		self.choosenkey = None

		self.getKey()

	def getKey(self):
		ldkeys = self.db_manager.selectAll("LuckyDip")

		keylist = list()

		for ldkey in ldkeys:
			tmp = LuckyDipKey()
			tmp.keyID = ldkey[0]
			tmp.service = ldkey[1]
			tmp.key = ldkey[2]
			keylist.append(tmp)

		if keyList.len() > 0:
			self.choosenkey = random.choice(keylist)
		else:
			self.choosenkey = None

	def deleteKeyFromDB(self):
		query = "DELETE FROM LuckyDip WHERE LuckyDipID=" + str(self.choosenkey.keyID)
		self.db_manager.executeQuery(query)

	def getOutput(self):
		if self.choosenkey is None:
			return "My one regret, is that I have but <no more> keys to give."
		else:
			self.deleteKeyFromDB()
			return "Free stuff! " + self.choosenkey.service + " key: " + self.choosenkey.key


class ManPage():
	def __init__(self, nick, full_msg, cmdList, cmdListStatic):
		self.nick = nick
		self.full_msg = full_msg
		self.cmdList = cmdList
		self.cmdListStatic = cmdListStatic

	def getOutput(self):
		for cmd in self.cmdList:
			if self.full_msg[1] in cmd.getInput():
				return "Usage: " + cmd.getUsage()

		if "!addcmd" in self.full_msg[1]:
			return "Usage: !addcmd !<CommandName>|Command Output (%s for arg)|Command Usage|<ModOnlyFlag>"
		elif "!listcmds" in self.full_msg[1]:
			return "Usage: !listcmds"
		elif "!man" in self.full_msg[1]:
			return "Usage: !man !<CommandName>"
		elif "!deletecmd" in self.full_msg[1]:
			return "Usage: !deletecmd !<CommandName>"
		elif "!editcmd" in self.full_msg[1]:
			return "Usage: !editcmd !<CommandName>|Command Output (%s for arg)|Command Usage>|<ModOnlyFlag>"
		elif "!raffle" in self.full_msg[1]:
			return "Usage: !raffle !<EnterCommand> <Time>"
		elif "!luckydip" in self.full_msg[1]:
			return "Usage: !luckydip"
		else:
			return "That is not a valid command."

class ListCommands():
	def __init__(self, nick, cmdList, cmdListStatic):
		self.nick = nick
		self.cmdList = cmdList
		self.cmdListStatic = cmdListStatic

	def getOutput(self):
		cmdListString = ""
		for cmd in self.cmdList:
			cmdListString = cmdListString + " " + cmd.getInput()
		for cmd in self.cmdListStatic:
			cmdListString = cmdListString + " " + cmd
		return cmdListString

class DeleteCommand():
	def __init__(self, nick, full_msg, db_manager, cmdList):
		self.nick = nick
		self.full_msg = full_msg
		self.db_manager = db_manager
		self.cmdList = cmdList

	def execute(self):
		if "!" in self.full_msg[1].split("|")[0]:
			query = "DELETE FROM Commands WHERE CommandInput='" + self.full_msg[1].split("|")[0] + "'"
			self.db_manager.executeQuery(query)
			self.cmdList[:] = []

	def getOutput(self):
		return self.full_msg[1] + " command deleted by " + self.nick


class AddCommand():
	def __init__(self, nick, full_msg, message, db_manager, cmdList):
		self.nick = nick
		self.full_msg = full_msg
		self.message = message
		self.db_manager = db_manager
		self.cmdList = cmdList

	def getOutput(self):
		print "Adding Command: " + str(self.full_msg)
		if "!" in self.full_msg[1]:
			if not self.db_manager.checkExistsByName("Commands", "CommandInput", self.full_msg[1].split("|")[0], "CommandInput", self.full_msg[1].split("|")[0]):
				print "Found Bang, Adding Command"
				line_split = self.message.split("|")
				tmpInput = self.full_msg[1].split("|")[0]
				tmpOutput = line_split[1]
				tmpUsage = line_split[2]
				tmpMod = line_split[3]
				query = "INSERT INTO Commands(CommandInput, CommandOutput, CommandUsage, ModCMD) VALUES ('" + tmpInput + "', '" + tmpOutput + "', '" + tmpUsage + "', " + tmpMod + ");"
				self.db_manager.executeQuery(query)
				tmpCmd = Command()
				tmpCmd.cmdInput = tmpInput
				tmpCmd.cmdOutput = tmpOutput
				tmpCmd.cmdUsage = tmpUsage
				self.cmdList.append(tmpCmd)
				return "New command added by " + self.nick + ": " + tmpInput			
