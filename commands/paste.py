#!/usr/bin/python
import urllib

class Paste():
	def __init__(self, config):
		self.apikey = config.getStr("pastebin_api_key")
		self.apiurl = "http://pastebin.com/api/api_post.php"

	def createPaste(self, message):
		pastebin_vars = {'api_dev_key':self.apikey,'api_option':'paste','api_paste_code':message}
		response = urllib.urlopen(self.apiurl, urllib.urlencode(pastebin_vars))
		return response.read()