#!/usr/bin/python
import time
import random

class RaffleManager():
	def __init__(self, config, connection):
		self.db_manager = config.db_manager
		self.connection = connection
		self.initRaffleVars()

	def initRaffleVars(self):
		self.activeRaffle = False
		self.raffleStartTime = None
		self.raffleTime = None
		self.raffleEnterCmd = None
		self.users = list()
		self.users[:] = []

	def startRaffle(self):
		self.activeRaffle = True
		self.raffleStartTime = time.time()
		self.connection.sendMessage("A new raffle has started! You have " + self.raffleTime + " seconds to enter with the command " + self.raffleEnterCmd)

	def endRaffle(self):
		if self.users:
			dedup = list(set(self.users))
			winner = random.choice(dedup)
			self.connection.sendMessage("The Winner is: " + winner)
		else:
			self.connection.sendMessage("There were no entrants")
		self.initRaffleVars()

	def update(self):
		if self.activeRaffle:
			if time.time() - self.raffleStartTime >= int(self.raffleTime):
				self.endRaffle()