#!/usr/bin/python

class Config():
	""" Config class to provide a blob of common config data

	This class contains methods and attributes used to centralize bits of the bot that may change.
	This prevents having to hunt through the code base to change a simple value.

	Attributes:
		host: The IRC Server to connect to
		port: The port to connect to on the IRC Server
		channel: The channel to join once connected to the IRC Server
		


	"""
        def __init__(self):
                ###LEGIT STUFF
                self.configFile = "bot.conf"
                self.configDict = dict()
                self.debug = True
                ####OBJ REFERNECS
                self.db_manager = None

                #Load config DICT from the config file
                self.loadConfig()

        def getStr(self, configKey):
                return self.configDict[configKey]

        def getInt(self, configKey):
                return int(self.getStr(configKey))

        def loadConfig(self):
                if self.debug:
                        print "Loading config from file"
                conf_handle = open(self.configFile, 'r')
                for configLine in conf_handle:
                        print "Loading line: " + configLine
                        if not configLine.startswith("#"):
                                tmp = configLine.strip('\n').split("=")
                                self.configDict[tmp[0]] = tmp[1]