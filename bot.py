#!/usr/bin/python
import irc
import db
import commands
import string
import re
import confman


config = confman.Config()
botDB = db.BotDB(config)
config.db_manager = botDB
connection = irc.IRCConnection(config)
connection.connect()
cmdMan = commands.CMDManager(config, connection)

def main():
    running = True
    while True:
        raw_data = connection.receiveData()
        if raw_data is not None:
            tmp_split = raw_data.split("\n")
            for data in tmp_split:
                print "Line: " + data
                (nick, message) = connection.parseMessage(data)
                if nick is not None:
                	if message is not None:
                		if config.debug == True:
                			print "MSG: " + message + " FROM: " + nick
                		handleMessage(nick, message)

       	update()

def handleMessage(nick, message):
	cmdManRtrn = cmdMan.parseMessage(nick, message)
    

def update():
	cmdMan.update()

if __name__ == "__main__":
    main()
