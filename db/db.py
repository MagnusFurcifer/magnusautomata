#!/usr/bin/python

import sqlite3

class BotDB:
	'''
	BotDB - Database Interface

	Table: Commands
	CommandID Int() PrimaryKey
	CommandInput VARCHAR(255)
	CommandOutput VARCHAR(255)
	'''
	def __init__(self, config):
		self.db_file = config.getStr("db_filename")
		self.db_con = None
		self.dbConnect()

	def dbConnect(self):
		self.db_con = sqlite3.connect(self.db_file)
		db_cursor = self.db_con.cursor()
		if not self.checkTableExist("Commands"):
			print "Commands Table not created"
			with self.db_con:
				db_cursor.execute("CREATE TABLE Commands (CommandID INTEGER PRIMARY KEY, CommandInput TEXT, CommandOutput TEXT, CommandUsage TEXT, ModCMD INTEGER)")
		if not self.checkTableExist("LuckyDip"):
			print "LuckyDip table not created"
			with self.db_con:
				db_cursor.execute("CREATE TABLE LuckyDip (LuckyDipID INTEGER PRIMARY KEY, LuckyDipService TEXT, LuckyDipKey TEXT)")

	def checkTableExist(self, table):
		db_cursor = self.db_con.cursor()
		query = "SELECT name FROM sqlite_master WHERE type='table' AND name='%s';" % table
		with self.db_con:
			db_cursor.execute(query)
			rtrn = db_cursor.fetchone()
			if rtrn is None:
				return False
			else:
				if table in rtrn:
					return True
				else:
					return False

	def selectAll(self, table):
		db_cursor = self.db_con.cursor()
		query = "SELECT * FROM %s;" % table
		return db_cursor.execute(query)

	def executeQuery(self, query):
		db_cursor = self.db_con.cursor()
		print "Executing This Query: " + query
		with self.db_con:
			db_cursor.execute(query)

	def checkExistsByName(self, table, testcol, coldata, retcol, expectedrtrn):
		db_cursor = self.db_con.cursor()
		query = "SELECT " + retcol + " FROM " + table + " WHERE " + testcol + "='" + coldata + "'"
		with self.db_con:
			db_cursor.execute(query)
			rtrn = db_cursor.fetchone()
			if rtrn is None:
				return False
			else:
				if expectedrtrn in rtrn:
					return True
				else:
					return False


