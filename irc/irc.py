#!/usr/bin/python
import socket
import string
import fcntl
import os
import errno

class IRCServer():
        def __init__(self, host, port, channel):
                self.host = host
                self.port = port
                self.channel = channel

class IRCCredentials():
        def __init__(self, nick, oauth_token):
                self.nick = nick
                self.oauth_token = oauth_token

class IRCSocket():
        def __init__(self):
                self.con = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        def connect(self, host, port):
                self.con.connect((host, int(port)))
                fcntl.fcntl(self.con, fcntl.F_SETFL, os.O_NONBLOCK)

        def send(self, message):
                self.con.send(message)

        def recv(self, bufferlen):
                try:
                        msg = self.con.recv(bufferlen)
                except socket.error, e:
                        err = e.args[0]
                        if err == errno.EAGAIN or err == errno.EWOULDBLOCK:
                                msg = None
                        else:
                                print "An actual socket error: " + e
                return msg

class IRCConnection():
        def __init__(self, config):
                self.IRCServer = IRCServer(config.getStr("host"), config.getInt("port"), config.getStr("channel"))
                self.IRCCredentials = IRCCredentials(config.getStr("nick"), config.getStr("oauth_token"))
                self.IRCSocket = IRCSocket()
                self.data = None
                self.mods = list()
                self.mods.append("magnusfurcifer")

        def getMods(self):
                return self.mods

        def connect(self):
                self.initSocket()
                self.authUser()
                self.joinChannel()

        def send(self, message):
                self.IRCSocket.send(message)

        def sendMessage(self, message):
                self.send("PRIVMSG %s :%s\r\n" % (self.IRCServer.channel, message))

        def checkPing(self, line):
                if "PING" in line[0]:
                        self.send('PONG %s\r\n' % line[1])

        def initSocket(self):
                self.IRCSocket.connect(self.IRCServer.host, self.IRCServer.port)

        def authUser(self):
                self.IRCSocket.send('PASS %s\r\n' % self.IRCCredentials.oauth_token)
                self.IRCSocket.send('NICK %s\r\n' % self.IRCCredentials.nick)

        def joinChannel(self):
                self.IRCSocket.send('JOIN %s\r\n' % self.IRCServer.channel)

        def receiveData(self):
                rtrnData = self.IRCSocket.recv(1024)
                if rtrnData is not None:
                        return rtrnData.rstrip()
                else:
                        return None

        def checkMods(self, line):
                if "jtv" in line[0]:
                        if "MODE" in line[1]:
                                if "+o" in line[3]:
                                        print "Adding: " + line[4] + " to Mods list"
                                        self.mods.append(line[4])

        def parseMessage(self, line):
                line=string.rstrip(line)
                line=string.split(line)
                self.checkPing(line)
                self.checkMods(line)
                if len(line) > 3:
                        nick = self.getNick(line)
                        message = self.getMessage(line)
                        return (nick, message)
                return (None, None)

        def getNick(self, line):
                user = None
                if line[1] == "PRIVMSG":
                        user = line[0]
                        user = user.split("!")
                        user = user[0]
                        user = user[1:]
                return user

        def getMessage(self, line):
                line.pop(0)
                line.pop(0)
                line.pop(0)
                line = ' '.join(line)
                return line.replace(":", "", 1)